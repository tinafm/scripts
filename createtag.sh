#!/bin/bash

board_version="P1A"

main_version=0

# minor_version=1:using featuredb getting tool 1.0 version 2017-08-17
minor_version=1
build_date=`date +%Y%m%d`
build_cnt=1
last_build_time=(`cat ../samedaybuildcnt.txt`)
last_build_date=${last_build_time[0]}
last_build_cnt=${last_build_time[1]}

if [[ ${last_build_date} == ${build_date} ]]; then
  # Build date is the same time, add the cnt
  let build_cnt=last_build_cnt+1
else
  # Build date is another day, init the cnt
  build_cnt=0
fi

echo "build_date is:${build_date}"
echo "build_cnt is:${build_cnt}"

echo ${build_date} > ../samedaybuildcnt.txt
echo ${build_cnt} >> ../samedaybuildcnt.txt
echo ${board_version} > ../boardversion.txt

tag_version="p${main_version}.${minor_version}.${build_date}v${build_cnt}"
echo ${tag_version} > ../tagversion.txt
echo ${tag_version} > ../package/nbr/ra/version.txt

IMG_NAME="nbrfs_${board_version}_${tag_version}"

echo "TAG version is:${tag_version}"
echo "IMA_NAME is:${IMG_NAME}"
